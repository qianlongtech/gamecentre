(function ($, Swiper) {
    "use strict";
    var GC = {},
        swiper;
    GC.ele = {
        body: $("body"),
        top: $(".top"),
        button: $(".button"),
        swiperWrapper: $(".swiper-wrapper"),
        titleWrapper: $(".title-wrapper"),
        descWrapper: $(".desc-wrapper"),
        swiperSlide: $(".swiper-slide"),
        title: $(".title"),
        desc: $(".desc")
    };
    GC.dom = {
        swiper: '<div class="swiper-slide"></div>',
        title: '<p class="title"></p>',
        desc: '<p class="desc"></p>'
    };
    GC.link = [];
    GC.getJson = function (callback) {
        $.ajax({
            url: "json/sys_data.json",
            method: "GET",
            dataType: "json",
            success: function (data) {
                callback(data);
            }
        });
    };
    GC.appendDom = function (data) {
        $.each(data, function (i, e) {
            GC.ele.swiperWrapper.append(
                '<div class="swiper-slide" style="background-image:url(' + e.imgURL + ')"></div>'
            );
            GC.ele.titleWrapper.append('<p class="title">' + e.title + '</p>');
            GC.ele.descWrapper.append('<p class="desc">' + e.desc +   '</p>');
            GC.link.push(e.link);
        });
    };
    GC.changeCSS = function () {
        var x = GC.ele.body.height() - GC.ele.top.height() - GC.ele.titleWrapper.height() - GC.ele.descWrapper.height() - 80;
        GC.ele.swiperSlide = $(".swiper-slide");
        GC.ele.swiperSlide.css({"height": x, "width": x});
    };
    GC.bindImgClick = function () {
        GC.ele.swiperSlide = $(".swiper-slide");
        GC.ele.swiperSlide.each(function (i, e) {
            $(e).click(function () {
                window.location.href = GC.link[i];
            });
        });
    };
    GC.onSlideChangeEnd = function (index) {
        GC.ele.title = $(".title");
        GC.ele.desc = $(".desc");
        GC.ele.title.hide().eq(index + 1).show();
        GC.ele.desc.hide().eq(index + 1).show();
        GC.ele.button[0].href = GC.link[index];
    };
    GC.setData = (function () {
        GC.getJson(function (data) {
            GC.appendDom(data);
            GC.changeCSS();
            GC.bindImgClick();
            swiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                effect: 'coverflow',
                grabCursor: true,
                centeredSlides: true,
                slidesPerView: 'auto',
                coverflow: {
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: true
                },
                onInit: function (swiper) {
                    GC.onSlideChangeEnd(0);
                },
                onSlideChangeEnd: function (swiper) {
                    GC.onSlideChangeEnd(swiper.activeIndex);
                }
            });
        });
    }());
}(window.jQuery, window.Swiper));